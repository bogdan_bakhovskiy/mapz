﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Interpreter
{
     class BackupFile
     {
         private BackupSettings BackupSetting { get; set; }
         public int TotalFiles;
         public int CopiedFiles;
         public int CopiedDirectories;

         public BackupFile(BackupSettings backup)
         {
             BackupSetting = backup;
         }

         public void BackupFiles()
         {
             string backupName = BackupSetting.Name;
             string backupSourceDirectory = BackupSetting.SourceDirectory;
             string backupDestinationDirectory = BackupSetting.DestinationDirectory;
             bool copySubDirectories = true;

             TotalFiles = Directory.GetFiles(backupSourceDirectory, "*", SearchOption.AllDirectories).Length;
             
             DirectoryCopy(backupSourceDirectory, backupDestinationDirectory, copySubDirectories);
         }

         private void DirectoryCopy(string backupSourceDirectory, string backupDestinationDirectory, bool copySubDirectories)
         {
             DirectoryInfo directory = new DirectoryInfo(backupSourceDirectory);
             if (!directory.Exists)
             {
                 throw new DirectoryNotFoundException(
                     "Source directory does not exist or could not be found: "
                     + backupSourceDirectory);
             }
             DirectoryInfo[] dirs = directory.GetDirectories();
             
             if (!Directory.Exists(backupDestinationDirectory))
             {
                 Directory.CreateDirectory(backupDestinationDirectory);
             }

             
             FileInfo[] files = directory.GetFiles();
             foreach (FileInfo file in files)
             {
                  for (int i = 0; i < 1; ++i)
                  {
                     string temppath = Path.Combine(backupDestinationDirectory, files.ElementAt(0).Name);
                     files.ElementAt(0).CopyTo(temppath, false);
                     CopiedFiles++;
                  }
             }

            
             if (copySubDirectories)
             {
                 foreach (DirectoryInfo subdir in dirs)
                 {
                     string temppath = Path.Combine(backupDestinationDirectory, subdir.Name);
                     DirectoryCopy(subdir.FullName, temppath, copySubDirectories);
                     CopiedDirectories++;
                 }
             }
         }
     }
     public class BackupSettings
     {
         public string Name { get; set; }
         public string DestinationDirectory { get; set; }
         public string SourceDirectory { get; set; }

         public BackupSettings(string name, string destinationDirectory, string sourceDirectory)
         {
             Name = name;
             DestinationDirectory = destinationDirectory;
             SourceDirectory = sourceDirectory;
         }
     }
}
