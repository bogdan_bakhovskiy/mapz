﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Language.AST;
using Language.Data;
using Language.Exceptions;
using Language.Other;
using Language.Lexers;

namespace Language.Parser
{
    public class LanguageParser
    {
        private ParseableTokenStream TokenStream { get; set; }
        public LanguageParser(Lexers.Lexer lexer)
        {
            TokenStream = new ParseableTokenStream(lexer);
        }
        public Ast Parse()
        {
            var statements = new List<Ast>(1024);
            while (TokenStream.Current.TokenType != TokenType.EOF)
            {
                statements.Add(ScopeStart().Or(Statement));
            }
            return new ScopeDeclr(statements);
        }
        private Ast GetIf()
        {
            if (TokenStream.Current.TokenType == TokenType.If)
            {
                return TokenStream.Capture(ParseIf);
            }
            return null;
        }
        private Condition ParseIf()
        {
            var predicateAndExpressions = GetPredicateAndStatements(TokenType.If);
            var predicate = predicateAndExpressions.Item1;
            var statements = predicateAndExpressions.Item2;
            if (TokenStream.Current.TokenType != TokenType.Else)
            {
                return new Condition(new Token(TokenType.If), predicate, statements);
            }
            if (TokenStream.Peek(1).TokenType == TokenType.If)
            {
                TokenStream.Take(TokenType.Else);
                var alternate = ParseIf();
                return new Condition(new Token(TokenType.If), predicate, statements, alternate);
            }
            return new Condition(new Token(TokenType.If), predicate, statements, ParseTrailingElse());
        }
        private Ast GetWhile()
        {
            if (TokenStream.Current.TokenType == TokenType.While)
            {
                Func<LoopWhile> op = () => {
                    var predicateAndStatements = GetPredicateAndStatements(TokenType.While);
                    var predicate = predicateAndStatements.Item1;
                    var statements = predicateAndStatements.Item2;
                    return new LoopWhile(predicate, statements);
                };
                return TokenStream.Capture(op);
            }
            return null;
        }


        private Condition ParseTrailingElse()
        {
            TokenStream.Take(TokenType.Else);
            var statements = GetStatementsInScope(TokenType.LBracket, TokenType.RBracket);
            return new Condition(new Token(TokenType.Else), null, statements);
        }
        private Ast GetFor()
        {
            if (TokenStream.Current.TokenType == TokenType.For )//&& TokenStream.Alt(ParseFor))
            {
                return TokenStream.Get(ParseFor);
            }
            return null;
        }

        private LoopFor ParseFor()
        {
            TokenStream.Take(TokenType.For);
            var args = GetArgumentList();
            var init = args[0];
            var condition = args[1];
            var modify = args[2];
            var body = GetStatementsInScope(TokenType.LBracket, TokenType.RBracket);
            return new LoopFor(init, condition, modify, body);
        }








        private Ast Statement()
        {
            var ast = TokenStream.Capture(MethodDeclaration);
            if (ast != null)
            {
                return ast;
            }
            var statement = InnerStatement();
            if (TokenStream.Current.TokenType == TokenType.SemiColon)
            {
                TokenStream.Take(TokenType.SemiColon);
            }
            return statement;
        }

        private Ast InnerStatement()
        {
            var ast = TryCatch().Or(ScopeStart)
                                  .Or(VariableDeclWithAssignStatement)
                                .Or(VariableDeclrStatement)
                                .Or(GetIf)
                                .Or(GetWhile)
                                .Or(GetFor)
                                .Or(GetReturn)
                                .Or(PrintStatement)
                                .Or(CreateStatement)
                                .Or(DeleteStatement)
                             .Or(MoveStatement)
                              .Or(CopyStatement)
                              .Or(FindMasksStatement)
                              .Or(FindSameFilesStatement)
                              .Or(BackupStatement)
                                .Or(Expression);


            if (ast != null)
            {
                return ast;
            }

            throw new InvalidSyntax(String.Format("Unknown expression type {0} - {1}", TokenStream.Current.TokenType, TokenStream.Current.TokenVal));
        }



        private Ast TryCatch()
        {

            return null;
        }
        private Ast New()
        {
            Func<Ast> op = () =>
            {
                //if (TokenStream.Current.TokenType == TokenType.New)
                //{
                //    TokenStream.Take(TokenType.New);

                //    if (ValidNewable())
                //    {
                //        var name = new Expr(TokenStream.Take(TokenStream.Current.TokenType));

                //        if (TokenStream.Current.TokenType == TokenType.LSquareBracket)
                //        {
                //            TokenStream.Take(TokenType.LSquareBracket);

                //            var size = Expression();

                //            TokenStream.Take(TokenType.RSquareBracket);

                //            return new NewArrayAst(name, size);
                //        }

                //        var args = GetArgumentList();

                //        return new NewAst(name, args);
                //    }
                //}

                //if (TokenStream.Current.TokenType == TokenType.OpenParenth &&
                //    TokenStream.Peek(1).TokenType == TokenType.New)
                //{
                //    TokenStream.Take(TokenType.OpenParenth);

                //    var item = New();

                //    TokenStream.Take(TokenType.CloseParenth);

                //    return item;
                //}

                return null;
            };

            return TokenStream.Capture(op);
        }

        private Ast ClassReferenceStatement()
        {
            Func<Ast> reference = () =>
            {
                var references = new List<Ast>();

                var classInstance = New().Or(() => new Expr(TokenStream.Take(TokenType.Word)));

                while (true)
                {
                    if (TokenStream.Current.TokenType == TokenType.Dot)
                    {
                        TokenStream.Take(TokenType.Dot);
                    }
                    else
                    {
                        if (references.Count == 0)
                        {
                            return null;
                        }


                    }

                    var deref = FunctionCallStatement().Or(() => TokenStream.Current.TokenType == TokenType.Word ? new Expr(TokenStream.Take(TokenType.Word)) : null);

                    references.Add(deref);
                }
                // return null;
            };

            return TokenStream.Capture(reference);
        }

        private Ast ScopeStart()
        {
            if (TokenStream.Current.TokenType == TokenType.LBracket)
            {
                var statements = GetStatementsInScope(TokenType.LBracket, TokenType.RBracket);

                return statements;
            }

            return null;
        }



        #region Print

        private Ast PrintStatement()
        {
            Func<Ast> op = () =>
            {
                TokenStream.Take(TokenType.Print);

                var expr = InnerStatement();

                if (expr != null)
                {
                    return new PrintAst(expr);
                }

                return null;
            };

            if (TokenStream.Alt(op))
            {
                return TokenStream.Get(op);
            }

            return null;
        }

        #endregion

        #region File operationss

        private Ast CreateStatement()
        {
            Func<Ast> op = () =>
            {
                TokenStream.Take(TokenType.Create);

                var expr = InnerStatement();

                if (expr != null)
                {
                    return new CreateAst(expr);
                }

                return null;
            };

            if (TokenStream.Alt(op))
            {
                return TokenStream.Get(op);
            }

            return null;
        }

        private Ast DeleteStatement()
        {
            Func<Ast> op = () =>
            {
                TokenStream.Take(TokenType.Delete);

                var expr = InnerStatement();

                if (expr != null)
                {
                    return new DeleteAst(expr);
                }

                return null;
            };

            if (TokenStream.Alt(op))
            {
                return TokenStream.Get(op);
            }

            return null;
        }

        private Ast MoveStatement()
        {
           

            Func<Ast> op = () =>
            {
                TokenStream.Take(TokenType.Move);

               // var expr = InnerStatement();
                List<Language.AST.Ast> arg = GetArgumentList();

                if (arg != null)
                {
                    return new Move(arg);
                }

                return null;
            };

            if (TokenStream.Alt(op))
            {
                return TokenStream.Get(op);
            }

            return null;
        }
        private Ast CopyStatement()
        {


            Func<Ast> op = () =>
            {
                TokenStream.Take(TokenType.Copy);

                // var expr = InnerStatement();
                List<Language.AST.Ast> arg = GetArgumentList();

                if (arg != null)
                {
                    return new Copy(arg);
                }

                return null;
            };

            if (TokenStream.Alt(op))
            {
                return TokenStream.Get(op);
            }

            return null;
        }
        private Ast FindMasksStatement()
        {


            Func<Ast> op = () =>
            {
                TokenStream.Take(TokenType.FindMasks);

                // var expr = InnerStatement();
                List<Language.AST.Ast> arg = GetArgumentList();

                if (arg != null)
                {
                    return new FindMasks(arg);
                }

                return null;
            };

            if (TokenStream.Alt(op))
            {
                return TokenStream.Get(op);
            }

            return null;
        }

        private Ast FindSameFilesStatement()
        {


            Func<Ast> op = () =>
            {
                TokenStream.Take(TokenType.FindSameFiles);

                // var expr = InnerStatement();
                List<Language.AST.Ast> arg = GetArgumentList();

                if (arg != null)
                {
                    return new FindSameFiles(arg);
                }

                return null;
            };

            if (TokenStream.Alt(op))
            {
                return TokenStream.Get(op);
            }

            return null;
        }

        private Ast BackupStatement()
        {


            Func<Ast> op = () =>
            {
                TokenStream.Take(TokenType.Backup);

                List<Language.AST.Ast> arg = GetArgumentList();

                if (arg != null)
                {
                    return new Backup(arg);
                }

                return null;
            };

            if (TokenStream.Alt(op))
            {
                return TokenStream.Get(op);
            }

            return null;
        }
        #endregion

        #region Expressions of single items or expr op expr

        private Ast Expression()
        {
            if (IsValidOperand())//|| TokenStream.Current.TokenType == TokenType.New)
            {
                return ParseExpression();
            }

            switch (TokenStream.Current.TokenType)
            {
                case TokenType.OpenParenth:

                    Func<Ast> basicOp = () =>
                    {
                        TokenStream.Take(TokenType.OpenParenth);

                        var expr = Expression();

                        TokenStream.Take(TokenType.CloseParenth);

                        return expr;
                    };

                    Func<Ast> doubleOp = () =>
                    {
                        var op1 = basicOp();

                        var op = Operator();

                        var expr = Expression();

                        return new Expr(op1, op, expr);
                    };

                    return TokenStream.Capture(doubleOp)
                                      .Or(() => TokenStream.Capture(basicOp));

                default:
                    return null;
            }
        }

        private Ast ParseExpression()
        {
            Func<Func<Ast>, Func<Ast>, Ast> op = (leftFunc, rightFunc) =>
            {
                var left = leftFunc();

                if (left == null)
                {
                    return null;
                }

                var opType = Operator();

                var right = rightFunc();

                if (right == null)
                {
                    return null;
                }

                return new Expr(left, opType, right);
            };

            Func<Ast> leftOp = () => op(ExpressionTerminal, Expression);

            return TokenStream.Capture(leftOp)
                              .Or(() => TokenStream.Capture(ExpressionTerminal));
        }

        #endregion

        #region Return

        private Ast GetReturn()
        {
            if (TokenStream.Current.TokenType == TokenType.Return && TokenStream.Alt(ParseReturn))
            {
                return TokenStream.Get(ParseReturn);
            }

            return null;
        }

        private ReturnAst ParseReturn()
        {
            TokenStream.Take(TokenType.Return);

            if (TokenStream.Current.TokenType == TokenType.SemiColon)
            {
                return new ReturnAst();
            }

            return new ReturnAst(InnerStatement());
        }


        #endregion



        #region Function parsing (lambdas, declarations, args)

        private Ast FunctionCall()
        {
            var name = TokenStream.Take(TokenType.Word);

            var args = GetArgumentList();

            return new FuncInvoke(name, args);
        }





        private Ast MethodDeclaration()
        {
            if (!IsValidMethodReturnType())
            {
                throw new InvalidSyntax("Invalid syntax");
            }

            // return type
            var returnType = TokenStream.Take(TokenStream.Current.TokenType);

            // func name
            var funcName = TokenStream.Take(TokenType.Word);

            var argList = GetArgumentList(true);

            var innerExpressions = GetStatementsInScope(TokenType.LBracket, TokenType.RBracket);

            return new MethodDeclr(returnType, funcName, argList, innerExpressions);
        }

        private List<Ast> GetArgumentList(bool includeType = false)
        {
            TokenStream.Take(TokenType.OpenParenth);

            var args = new List<Ast>(64);

            while (TokenStream.Current.TokenType != TokenType.CloseParenth)
            {
                var argument = includeType ? VariableDeclaration() : InnerStatement();

                args.Add(argument);

                if (TokenStream.Current.TokenType == TokenType.Comma || TokenStream.Current.TokenType == TokenType.SemiColon)
                {
                    TokenStream.Take(TokenStream.Current.TokenType);
                }
            }

            TokenStream.Take(TokenType.CloseParenth);

            return args;
        }

        #endregion

        #region Variable Declrations and Assignments

        private Ast VariableDeclarationAndAssignment()
        {
            var isVar = TokenStream.Current.TokenType == TokenType.Infer;

            if ((isVar || IsValidMethodReturnType()) && IsValidVariableName(TokenStream.Peek(1)))
            {
                var type = TokenStream.Take(TokenStream.Current.TokenType);

                var name = TokenStream.Take(TokenType.Word);

                TokenStream.Take(TokenType.Equals);

                bool isLink = false;
                if (TokenStream.Current.TokenType == TokenType.Ampersand)
                {
                    isLink = true;
                    TokenStream.Take(TokenType.Ampersand);
                }

                var expr = InnerStatement();

                expr.IsLink = isLink;

                return new VarDeclrAst(type, name, expr);
            }

            return null;
        }

        private Ast VariableDeclaration()
        {
            if (IsValidMethodReturnType() && IsValidVariableName(TokenStream.Peek(1)))
            {
                var type = TokenStream.Take(TokenStream.Current.TokenType);

                var name = TokenStream.Take(TokenType.Word);

                if (TokenStream.Current.TokenType != TokenType.LSquareBracket)
                {
                    return new VarDeclrAst(type, name);
                }

                //TokenStream.Take(TokenType.LSquareBracket);

                //TokenStream.Take(TokenType.RSquareBracket);

                //return new ArrayDeclrAst(type, name);
            }

            return null;
        }

        private Ast VariableAssignment()
        {
            var name = TokenStream.Take(TokenType.Word);

            var equals = TokenStream.Take(TokenType.Equals);

            return new Expr(new Expr(name), equals, InnerStatement());
        }

        #endregion

        #region Single Expressions or Tokens

        private Ast ExpressionTerminal()
        {
            return ClassReferenceStatement().Or(FunctionCallStatement)
                                            .Or(VariableAssignmentStatement)
                                            //.Or(New)
                                            .Or(SingleToken);
        }



        private Ast SingleToken()
        {
            if (IsValidOperand())
            {
                var token = new Expr(TokenStream.Take(TokenStream.Current.TokenType));

                return token;
            }

            return null;
        }

        private Token Operator()
        {
            if (TokenUtil.IsOperator(TokenStream.Current))
            {
                return TokenStream.Take(TokenStream.Current.TokenType);
            }

            throw new InvalidSyntax(String.Format("Invalid token found. Expected operator but found {0} - {1}", TokenStream.Current.TokenType, TokenStream.Current.TokenVal));
        }

        #endregion

        #region Helpers

        private Tuple<Ast, ScopeDeclr> GetPredicateAndStatements(TokenType type)
        {
            TokenStream.Take(type);

            TokenStream.Take(TokenType.OpenParenth);

            var predicate = Expression();

            TokenStream.Take(TokenType.CloseParenth);

            var statements = GetStatementsInScope(TokenType.LBracket, TokenType.RBracket);

            return new Tuple<Ast, ScopeDeclr>(predicate, statements);
        }

        private ScopeDeclr GetStatementsInScope(TokenType open, TokenType close, bool expectSemicolon = true)
        {
            return GetStatementsInScope(open, close, InnerStatement, expectSemicolon);
        }

        private ScopeDeclr GetStatementsInScope(TokenType open, TokenType close, Func<Ast> getter, bool expectSemicolon = true)
        {
            TokenStream.Take(open);
            var lines = new List<Ast>();
            while (TokenStream.Current.TokenType != close)
            {
                var statement = getter();

                lines.Add(statement);

                if (expectSemicolon && StatementExpectsSemiColon(statement))
                {
                    TokenStream.Take(TokenType.SemiColon);
                }
            }

            TokenStream.Take(close);

            return new ScopeDeclr(lines);
        }


        private bool StatementExpectsSemiColon(Ast statement)
        {
            return !(statement is MethodDeclr ||
                     statement is Condition ||
                     statement is LoopWhile ||

                     statement is LoopFor);
            //||
            //statement is TryCatchAst);
        }

        #endregion





        private Ast VariableDeclWithAssignStatement()
        {
            return TokenStream.Capture(VariableDeclarationAndAssignment);
        }

        private Ast VariableAssignmentStatement()
        {
            return TokenStream.Capture(VariableAssignment);
        }

        private Ast VariableDeclrStatement()
        {
            return TokenStream.Capture(VariableDeclaration);
        }

        private Ast FunctionCallStatement()
        {
            return TokenStream.Capture(FunctionCall);
        }



        private bool IsValidMethodReturnType()
        {
            switch (TokenStream.Current.TokenType)
            {
                case TokenType.Void:
                case TokenType.Word:
                case TokenType.Int:
                case TokenType.String:
                case TokenType.Infer:
                case TokenType.Method:
                case TokenType.Boolean:
                    return true;
            }
            return false;
        }

        private bool IsValidOperand()
        {
            switch (TokenStream.Current.TokenType)
            {
                case TokenType.Int:
                case TokenType.QuotedString:
                case TokenType.Word:
                case TokenType.True:
                case TokenType.Float:

                case TokenType.False:
                    return true;
            }
            return false;
        }

        private bool ValidNewable()
        {
            switch (TokenStream.Current.TokenType)
            {
                case TokenType.Int:
                case TokenType.String:
                case TokenType.Word:
                case TokenType.Float:
                case TokenType.Boolean:
                    return true;
            }
            return false;
        }

        private bool IsValidVariableName(Token item)
        {
            switch (item.TokenType)
            {
                case TokenType.Word:
                    return true;
            }
            return false;
        }





    }

    internal class Memo
    {
        public Ast Ast { get; set; }
        public int NextIndex { get; set; }
    }

    public class ParseableTokenStream : TokenizableStreamBase<Token>
    {
        public ParseableTokenStream(Lexer lexer) : base(() => lexer.Lex().ToList())
        {
        }

        private Dictionary<int, Memo> CachedAst = new Dictionary<int, Memo>();

        public Boolean IsMatch(TokenType type)
        {
            if (Current.TokenType == type)
            {
                return true;
            }

            return false;
        }

        public Ast Capture(Func<Ast> ast)
        {
            if (Alt(ast))
            {
                return Get(ast);
            }

            return null;
        }

        /// <summary>
        /// Retrieves a cached version if it was found during any alternate route
        /// otherwise executes it
        /// </summary>
        /// <param name="getter"></param>
        /// <returns></returns>
        public Ast Get(Func<Ast> getter)
        {
            Memo memo;
            if (!CachedAst.TryGetValue(Index, out memo))
            {
                return getter();
            }

            Index = memo.NextIndex;

            //Console.WriteLine("Returning type {0} from index {1} as memo", memo.Ast, Index);
            return memo.Ast;
        }

        public Token Take(TokenType type)
        {
            if (IsMatch(type))
            {
                var current = Current;

                Consume();

                return current;
            }

            throw new InvalidSyntax(String.Format("Invalid Syntax. Expecting {0} but got {1}", type, Current.TokenType));
        }




        public Boolean Alt(Func<Ast> action)
        {
            TakeSnapshot();

            Boolean found = false;

            try
            {
                var currentIndex = Index;

                var ast = action();

                if (ast != null)
                {
                    found = true;

                    CachedAst[currentIndex] = new Memo
                    {
                        Ast = ast,
                        NextIndex = Index
                    };
                }
            }
            catch
            {

            }

            RollbackSnapshot();

            return found;
        }

        public override Token Peek(int lookahead)
        {
            var peeker = base.Peek(lookahead);

            if (peeker == null)
            {
                return new Token(TokenType.EOF);
            }

            return peeker;
        }

        public override Token Current
        {
            get
            {
                var current = base.Current;
                if (current == null)
                {
                    return new Token(TokenType.EOF);
                }
                return current;
            }
        }
    }
}
