﻿using System;
using System.Collections.Generic;
using System.Linq;
using Language.Data;
using Language.Executer;
using Language.Other;

namespace Language.AST
{
    public class Condition : Ast
    {

        public ScopeDeclr Body { get; set; }

        public Ast Predicate { get; set; }

        public Condition Alternate { get; set; }

        public Condition(Token token) : base(token)
        {
        }

        public Condition(Token conditionalType, Ast predic, ScopeDeclr body, Condition alternate = null)
            : this(conditionalType)
        {
            Predicate = predic;
            Body = body;
            Alternate = alternate;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Conditional; }
        }

        public override string ToString()
        {
            return "(" + Token + "(" + Predicate + ") then " + Body + (Alternate != null ? " else " + Alternate : "");
        }
    }
    public class LoopFor : Ast
    {
        public Ast Setup { get; private set; }

        public Ast Predicate { get; private set; }

        public Ast Update { get; private set; }

        public ScopeDeclr Body { get; private set; }

        public LoopFor(Ast init, Ast stop, Ast modify, ScopeDeclr body)
            : base(new Token(TokenType.For))
        {
            Setup = init;

            Predicate = stop;

            Update = modify;

            Body = body;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.For; }
        }

        public override string ToString()
        {
            return "(" + Token + "(" + Setup + ") (" + Predicate + ")" + "(" + Update + "){" + Body + "}";
        }
    }
    public class LoopWhile : Ast
    {
        public Ast Predicate { get; private set; }

        public ScopeDeclr Body { get; private set; }

        public LoopWhile(Token token) : base(token)
        {
        }

        public LoopWhile(Ast predic, ScopeDeclr body)
            : this(new Token(TokenType.While))
        {
            Predicate = predic;
            Body = body;
        }


        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.While; }
        }

        public override string ToString()
        {
            return "While (" + Predicate + ") do " + Body.ScopedStatements.Aggregate("", (acc, item) => acc + item + ",");
        }
    }

    public class Backup : Ast
    {
        public List<Ast> Arguments { get; private set; }


        public Backup(List<Ast> expression) : base(new Token(TokenType.Backup))
        {
            Arguments = expression;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Backup; }
        }

    }

    public class PrintAst : Ast
    {
        public Ast Expression { get; private set; }
        public PrintAst(Ast expression) : base(new Token(TokenType.Print))
        {
            Expression = expression;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Print; }
        }
    }

    public class CreateAst : Ast
    {
        public Ast Expression { get; private set; }
        public CreateAst(Ast expression) : base(new Token(TokenType.Print))
        {
            Expression = expression;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Create; }
        }
    }
    public class DeleteAst : Ast
    {
        public Ast Expression { get; private set; }
        public DeleteAst(Ast expression) : base(new Token(TokenType.Delete))
        {
            Expression = expression;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Delete; }
        }
    }
    

    public class Move : Ast
    {
        public List<Ast> Arguments { get; private set; }

        
        public Move(List<Ast> expression) : base(new Token(TokenType.Move))
        {
            Arguments = expression;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Move; }
        }

    }

    public class Copy : Ast
    {
        public List<Ast> Arguments { get; private set; }


        public Copy(List<Ast> expression) : base(new Token(TokenType.Copy))
        {
            Arguments = expression;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Copy; }
        }

    }

     public class FindMasks : Ast
    {
        public List<Ast> Arguments { get; private set; }


        public FindMasks(List<Ast> expression) : base(new Token(TokenType.FindMasks))
        {
            Arguments = expression;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.FindMasks; }
        }

    }

    public class FindSameFiles : Ast
    {
        public List<Ast> Arguments { get; private set; }


        public FindSameFiles(List<Ast> expression) : base(new Token(TokenType.FindSameFiles))
        {
            Arguments = expression;
        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.FindSameFiles; }
        }

    }

 

    public abstract class Ast : IAcceptExecuter
    {
        public MemorySpace CallingMemory { get; set; }

        public Scope CallingScope { get; set; }

        public Scope CurrentScope { get; set; }

        public Scope Global { get; set; }

        public Token Token { get; set; }

        public IType AstSymbolType { get; set; }

        public List<Ast> Children { get; private set; }

        public Ast ConvertedExpression { get; set; }

        public bool IsLink { get; set; }

        public Ast(Token token)
        {
            Token = token;
            Children = new List<Ast>();
        }

        public void AddChild(Ast child)
        {
            if (child != null)
            {
                Children.Add(child);
            }
        }

        public override string ToString()
        {
            return Token.TokenType + " " + Children.Aggregate("", (acc, ast) => acc + " " + ast);
        }

        public abstract void Execute(IAstExecuter executer);

        /// <summary>
        /// Used instead of reflection to determine the syntax tree type
        /// </summary>
        public abstract AstTypes AstType { get; }

        /// <summary>
        /// Pure dynamic's are resolved only at runtime
        /// </summary>
        public bool IsPureDynamic { get; set; }
    }

    public enum AstTypes
    {
      
        Conditional,
        ClassRef,
        ScopeDeclr,
        Print,
        Expression,
        For,
        FunctionInvoke,
        MethodDeclr,
        Return,
        VarDeclr,
        While,
        Create,
        Delete,
        Copy,
        Move,
        FindMasks,
        FindSameFiles,
        Backup
    }

    public class Expr : Ast
    {
        public Ast Left { get; private set; }

        public Ast Right { get; private set; }

        public Expr(Token token)
            : base(token)
        {
        }

        public Expr(Ast left, Token token, Ast right)
            : base(token)
        {
            Left = left;
            Right = right;
        }


        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Expression; }
        }

        public override string ToString()
        {
            if (Left == null && Right == null)
            {
                return Token.ToString();
            }

            return "(" + Left + " " + Token + " " + Right + ")";
        }
    }

    public class FuncInvoke : Ast
    {
        public List<Ast> Arguments { get; private set; }

        public Ast FunctionName { get; set; }

        public IType ReturnType { get; set; }

        public FuncInvoke(Token token, List<Ast> args) : base(token)
        {
            FunctionName = new Expr(token);

            Arguments = args;
        }


        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.FunctionInvoke; }
        }

        public override string ToString()
        {
            return "call " + FunctionName + " with args " + Arguments.Aggregate("", (acc, item) => acc + item + ",");
        }
    }

    public class MethodDeclr : Ast
    {
        public Ast MethodName { get; private set; }

        /// <summary>
        /// An expression representing the return type declared for the method
        /// </summary>
        public Ast MethodReturnType { get; private set; }

        public List<Ast> Arguments { get; private set; }

        public ScopeDeclr Body { get; private set; }

        public Boolean IsAnonymous { get; set; }

        public ReturnAst ReturnAst { get; set; }

        public MethodDeclr(Token returnType, Token funcName, List<Ast> args, ScopeDeclr body, bool isAnon = false)
            : base(funcName)
        {
            MethodReturnType = new Expr(returnType);

            MethodName = new Expr(funcName);

            Arguments = args;

            Body = body;

            IsAnonymous = isAnon;
        }


        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.MethodDeclr; }
        }

        public override string ToString()
        {
            return "Declare " + MethodName + " ret: " + MethodReturnType + ", args " + Arguments.Aggregate("", (a, b) => a + b + ",") + " with body " + Body.ScopedStatements.Aggregate("", (acc, item) => acc + item + ",");
        }
    }

    public class ReturnAst : Ast
    {
        public Ast ReturnExpression { get; private set; }
        public ReturnAst(Ast expression) : base(new Token(TokenType.Return))
        {
            ReturnExpression = expression;
        }

        public ReturnAst()
            : base(new Token(TokenType.Return))
        {

        }

        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.Return; }
        }
    }

    public class ScopeDeclr : Ast
    {
        public List<Ast> ScopedStatements { get; private set; }

        public ScopeDeclr(List<Ast> statements) : base(new Token(TokenType.ScopeStart))
        {
            ScopedStatements = statements;
        }


        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.ScopeDeclr; }
        }

        public override string ToString()
        {
            return "SCOPE: \n" + ScopedStatements.Aggregate("", (item, acc) => item + acc + "\n");
        }
    }

    public class VarDeclrAst : Ast
    {
        public bool IsArray { get; set; }

        public Ast DeclarationType { get; private set; }

        public Ast VariableValue { get; private set; }

        public Ast VariableName { get; private set; }

        protected VarDeclrAst(Token token) : base(token)
        {
        }

        public VarDeclrAst(Token declType, Token name)
            : base(name)
        {
            DeclarationType = new Expr(declType);

            VariableName = new Expr(name);
        }

        public VarDeclrAst(Token declType, Token name, Ast value)
            : base(name)
        {
            DeclarationType = new Expr(declType);

            VariableValue = value;

            VariableName = new Expr(name);
        }


        public override void Execute(IAstExecuter executer)
        {
            executer.Execute(this);
        }

        public override AstTypes AstType
        {
            get { return AstTypes.VarDeclr; }
        }

        public override string ToString()
        {
            return String.Format("Declare {0} as {1} with value {2}",
                                 VariableName, DeclarationType, VariableValue);
        }
    }



}
