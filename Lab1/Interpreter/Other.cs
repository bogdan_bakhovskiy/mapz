﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Language.AST;
using Language.Data;

namespace Language.Other
{
    public interface IScopeable<T> where T : class, new()
    {
        void SetParentScope(T scope);
        List<IScopeable<T>> ChildScopes { get; }
    }

    public class MemorySpace : IScopeable<MemorySpace>
    {
        public Dictionary<string, object> Values { get; set; }

        public Dictionary<string, string> Links { get; set; }

        public MemorySpace EnclosingSpace { get; private set; }

        public MemorySpace()
        {
            Values = new Dictionary<string, object>();

            ChildScopes = new List<IScopeable<MemorySpace>>(64);

            Links = new Dictionary<string, string>();
        }

        public void Define(string name, object value)
        {
            Values[name] = value;
        }

        public void Link(string target, string source)
        {
            Links[target] = source;
        }

        public void Assign(string name, object value)
        {
            string link;
            while (Links.TryGetValue(name, out link))
            {
                name = link;
            }

            if (Values.ContainsKey(name))
            {
                Values[name] = value;

                return;
            }

            if (EnclosingSpace != null)
            {
                EnclosingSpace.Assign(name, value);
            }
            else
            {
                throw new Exception(
                    String.Format(
                        "Attempting to update variable {0} with value {1} but varialbe isn't defined in any memory scope",
                        name, value));
            }
        }

        public object Get(string name, bool local = false)
        {
            string link;

            if (Links.TryGetValue(name, out link))
            {
                return Get(link);
            }

            object o;
            if (Values.TryGetValue(name, out o))
            {
                return o;
            }

            if (EnclosingSpace != null && local == false)
            {
                return EnclosingSpace.Get(name);
            }

            return null;
        }

        public void SetParentScope(MemorySpace scope)
        {
            EnclosingSpace = scope;
        }

        public List<IScopeable<MemorySpace>> ChildScopes { get; private set; }
    }


    [Serializable]
    public class Scope : IScopeable<Scope>
    {
        public Dictionary<string, Symbol> Symbols { get; set; }

        public Scope EnclosingScope { get; private set; }

        public Boolean AllowAllForwardReferences { get; set; }

        public void SetParentScope(Scope scope)
        {
            EnclosingScope = scope;
        }

        public List<IScopeable<Scope>> ChildScopes { get; private set; }

        public Scope()
        {
            Symbols = new Dictionary<string, Symbol>();

            ChildScopes = new List<IScopeable<Scope>>(64);

            AllowAllForwardReferences = false;
        }

        public String ScopeName { get; set; }

        public void Define(Symbol symbol)
        {
            Symbols[symbol.Name] = symbol;
        }

        public Boolean AllowedForwardReferences(Ast ast)
        {
            if (Symbols.ContainsKey(ast.Token.TokenVal))
            {
                return AllowAllForwardReferences;
            }

            if (EnclosingScope == null)
            {
                return false;
            }

            return EnclosingScope.AllowedForwardReferences(ast);
        }

        public Symbol Resolve(Ast ast)
        {
            return Resolve(ast.Token.TokenVal);
        }

        public Symbol Resolve(String name)
        {
            Symbol o;
            if (Symbols.TryGetValue(name, out o))
            {
                return o;
            }

            if (EnclosingScope == null)
            {
                return null;
            }

            return EnclosingScope.Resolve(name);
        }
    }

    public enum ScopeType
    {
        Class,
        Global
    }
    public class ScopeContainer
    {
        public ScopeStack<Scope> Global { get; set; }

        public ScopeStack<Scope> Class { get; set; }

        public ScopeContainer()
        {
            Global = new ScopeStack<Scope>();

            Class = new ScopeStack<Scope>();

            CurrentScopeType = ScopeType.Global;
        }

        public ScopeType CurrentScopeType { get; set; }

        public ScopeStack<Scope> CurrentScopeStack
        {
            get
            {
                switch (CurrentScopeType)
                {
                    case ScopeType.Class:
                        return Class;
                    case ScopeType.Global:
                        return Global;
                }

                return null;
            }
        }
    }

    public class ScopeStack<T> where T : class, IScopeable<T>, new()
    {
        private Stack<T> Stack { get; set; }

        public T Current { get; set; }

        public ScopeStack()
        {
            Stack = new Stack<T>();
        }

        public void CreateScope()
        {
            var parentScope = Current;
            if (Current != null)
            {
                Stack.Push(Current);
            }
            Current = new T();
            Current.SetParentScope(parentScope);
            if (parentScope != null)
            {
                parentScope.ChildScopes.Add(Current);
            }
        }

        public void PopScope()
        {
            if (Stack.Count > 0)
            {
                Current = Stack.Pop();
            }
        }
    }







    [Serializable]
    public class BuiltInType : Symbol, IType
    {
        public BuiltInType(ExpressionTypes type, Ast src = null)
            : base(type.ToString())
        {
            ExpressionType = type;

            Src = src;
        }

        public string TypeName
        {
            get { return Name; }
        }

        public ExpressionTypes ExpressionType { get; set; }

        public Ast Src { get; set; }
    }

    [Serializable]
    public class ClassSymbol : Symbol, IType
    {
        public ClassSymbol(string name)
            : base(name, new UserDefinedType(name))
        {
            ExpressionType = ExpressionTypes.UserDefined;
        }

        public string TypeName
        {
            get { return Name; }
        }

        public ExpressionTypes ExpressionType { get; set; }

        public Ast Src { get; set; }
    }

    public enum ExpressionTypes
    {
        Int,
        String,
        UserDefined,
        Void,
        Boolean,
        Float,
        Inferred,
        Method,
        Nil
    }

    public interface IType
    {
        String TypeName { get; }
        ExpressionTypes ExpressionType { get; }
        Ast Src { get; set; }
    }

    [Serializable]
    public class MethodSymbol : Symbol
    {
        public MethodDeclr MethodDeclr { get; private set; }

        public MemorySpace Environment { get; set; }

        public MethodSymbol(string name, IType returnType, MethodDeclr declr)
            : base(name, returnType)
        {
            MethodDeclr = declr;
        }
    }

    [Serializable]
    public class Symbol : Scope
    {
        public String Name { get; private set; }
        public IType Type { get; private set; }

        public bool IsArray { get; set; }

        public Symbol(String name, IType type)
        {
            Name = name;
            Type = type;
        }

        public Symbol(String name)
        {
            Name = name;
        }
    }

    [Serializable]
    public class UserDefinedType : Symbol, IType
    {
        public UserDefinedType(string name) : base(name)
        {
            ExpressionType = ExpressionTypes.UserDefined;
        }

        public string TypeName
        {
            get { return Name; }
        }

        public ExpressionTypes ExpressionType { get; set; }
        public Ast Src { get; set; }
    }

    public static class CollectionUtil
    {
        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            foreach (var i in list)
            {
                action(i);
            }
        }

        public static Boolean IsNullOrEmpty<T>(ICollection<T> collection)
        {
            return collection == null || collection.Count == 0;
        }
    }

    public static class Maybe
    {
        public static TInput Or<TInput>(this TInput input, Func<TInput> evaluator)
            where TInput : class
        {
            if (input != null)
            {
                return input;
            }

            return evaluator();
        }
    }


    public static class NullTester
    {
        public static bool NullEqual(dynamic left, dynamic right)
        {
            if (left is TokenType && right is TokenType)
            {
                return left == TokenType.Nil && right == TokenType.Nil;
            }

            return false;
        }
    }

    class ScopeUtil
    {
        /// <summary>
        /// Determines user type
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static IType GetExpressionType(Ast left, Ast right, Token token)
        {
            switch (token.TokenType)
            {
                case TokenType.Ampersand:
                case TokenType.Or:
                case TokenType.GreaterThan:
                case TokenType.LessThan:
                    return new BuiltInType(ExpressionTypes.Boolean);

                case TokenType.Infer:
                    return right.AstSymbolType;
            }

            if (left.AstSymbolType.ExpressionType != right.AstSymbolType.ExpressionType)
            {
                throw new Exception("Mismatched types");
            }

            return left.AstSymbolType;
        }

        public static IType CreateSymbolType(Ast astType)
        {
            if (astType == null)
            {
                return null;
            }

            Func<IType> op = () =>
            {
                switch (astType.Token.TokenType)
                {
                    case TokenType.Int:
                        return new BuiltInType(ExpressionTypes.Int);
                    case TokenType.Float:
                        return new BuiltInType(ExpressionTypes.Float);
                    case TokenType.Void:
                        return new BuiltInType(ExpressionTypes.Void);
                    case TokenType.Infer:
                        return new BuiltInType(ExpressionTypes.Inferred);
                    case TokenType.QuotedString:
                    case TokenType.String:
                        return new BuiltInType(ExpressionTypes.String);
                    case TokenType.Word:
                        return new UserDefinedType(astType.Token.TokenVal);
                    case TokenType.Boolean:
                    case TokenType.False:
                    case TokenType.True:
                        return new BuiltInType(ExpressionTypes.Boolean);
                    case TokenType.Method:
                        return new BuiltInType(ExpressionTypes.Method);
                }
                return null;
            };

            var type = op();

            if (type != null)
            {
                type.Src = astType;
            }

            return type;
        }

        public static Symbol DefineUserSymbol(Ast ast, Ast name)
        {
            IType type = CreateSymbolType(ast);

            return new Symbol(name.Token.TokenVal, type);
        }

        public static Symbol DefineUserSymbol(IType type, Ast name)
        {
            return new Symbol(name.Token.TokenVal, type);
        }

        public static Symbol DefineMethod(MethodDeclr method)
        {
            IType returnType = CreateSymbolType(method.MethodReturnType);

            return new MethodSymbol(method.Token.TokenVal, returnType, method);
        }

        //public static Symbol DefineClassSymbol(ClassAst ast)
        //{
        //    return new ClassSymbol(ast.Token.TokenVal)
        //    {
        //        Src = ast,
        //        ScopeName = ast.Token.TokenVal
        //    };
        //}
    }


    public static class TokenUtil
    {
        public static Boolean EqualOrPromotable(IType item1, IType item2)
        {
            return EqualOrPromotable(item1.ExpressionType, item2.ExpressionType);
        }

        public static Boolean IsOperator(Token item)
        {
            switch (item.TokenType)
            {
                case TokenType.Equals:
                case TokenType.Plus:
                case TokenType.Minus:
                case TokenType.Asterix:
                case TokenType.Carat:
                case TokenType.GreaterThan:
                case TokenType.LessThan:
                case TokenType.Compare:
                case TokenType.NotCompare:
                case TokenType.Ampersand:
                case TokenType.Or:
                case TokenType.Slash:
                    return true;
            }
            return false;
        }

        public static bool EqualOrPromotable(ExpressionTypes item1, ExpressionTypes item2)
        {
            return item1 == item2 ||
                   item1 == ExpressionTypes.Method ||
                   item2 == ExpressionTypes.Method ||
                   item1 == ExpressionTypes.Inferred ||
                   item2 == ExpressionTypes.Inferred ||
                   item2 == ExpressionTypes.Nil || item1 == ExpressionTypes.Nil;
        }
    }
}
