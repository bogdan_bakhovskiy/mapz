﻿using System;
using System.Collections.Generic;
using System.Linq;
using Language.AST;
using Language.Data;
using Language.Exceptions;
using Language.Other;
using Language.Lexers;
using Language.Matches;
using Language.Parser;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Windows.Forms;
using Interpreter;
using System.Collections;
using System.Threading.Tasks;


namespace Language.Executer
{
    interface IAcceptExecuter
    {
        void Execute(IAstExecuter executer);
    }
    public interface IAstExecuter
    {
        void Execute(Condition ast);
        void Execute(CreateAst ast);
        void Execute(DeleteAst ast);
        void Execute(Copy ast);
        void Execute(Move ast);
        void Execute(FindMasks ast);
        void Execute(FindSameFiles ast);
        void Execute(Backup ast);
        void Execute(Expr ast);
        void Execute(FuncInvoke ast);
        void Execute(VarDeclrAst ast);
        void Execute(MethodDeclr ast);
        void Execute(LoopWhile ast);
        void Execute(ScopeDeclr ast);
        void Execute(LoopFor ast);
        void Execute(ReturnAst ast);
        void Execute(PrintAst ast);

        void Start(Ast ast);

    }

  


    public class InterpretorExecuter : IAstExecuter
    {


        private ScopeStack<MemorySpace> MemorySpaces { get; set; }

        public Stack<MemorySpace> Environment { get; set; }

        public MemorySpace Global { get; set; }

     



        public InterpretorExecuter()
        {
        
            Environment = new Stack<MemorySpace>();
            MemorySpaces = new ScopeStack<MemorySpace>();
              
        }


        public void Execute(Condition ast)
        {
            Exec(ast);
        }

        public void Execute(Expr ast)
        {
            Exec(ast);
        }

        public void Execute(FuncInvoke ast)
        {
            Exec(ast);
        }
        public void Execute(Move ast)
        {
            Exec(ast);
        }
        public void Execute(Copy ast)
        {
            Exec(ast);
        }
        public void Execute(FindMasks ast)
        {
            Exec(ast);
        }
        public void Execute(FindSameFiles ast)
        {
            Exec(ast);
        }
        public void Execute(Backup ast)
        {
            Exec(ast);
        }
        public void Execute(VarDeclrAst ast)
        {
            Exec(ast);
        }

        public void Execute(MethodDeclr ast)
        {
            Exec(ast);
        }

        public void Execute(LoopWhile ast)
        {
            Exec(ast);
        }

        public void Execute(ScopeDeclr ast)
        {
            Exec(ast);
        }

        public void Execute(LoopFor ast)
        {
            Exec(ast);
        }

        public void Execute(ReturnAst ast)
        {
            Exec(ast);
        }

        public void Execute(PrintAst ast)
        {
            Exec(ast);
        }

        public void Execute(CreateAst ast)
        {
            Exec(ast);
        }
        public void Execute(DeleteAst ast)
        {
            Exec(ast);
        }


        public void Start(Ast ast)
        {
            var scopeBuilder = new ScopeBuilderExecuter();
            var resolver = new ScopeBuilderExecuter(true);
            scopeBuilder.Start(ast);
            resolver.Start(ast);
            Global = MemorySpaces.Current;
            ast.Execute(this);


        }

        private void ForDo(LoopFor forLoop)
        {
            MemorySpaces.CreateScope();
            Exec(forLoop.Setup);
            while (GetValue(Exec(forLoop.Predicate)))
            {
                Exec(forLoop.Update);
                Exec(forLoop.Body);
            }
            MemorySpaces.PopScope();
        }

        private void WhileDo(LoopWhile whileLoop)
        {
            MemorySpaces.CreateScope();
            while (GetValue(Exec(whileLoop.Predicate)))
            {
                whileLoop.Body.ScopedStatements.ForEach(statement => Exec(statement));
            }
            MemorySpaces.PopScope();
        }

        private void ConditionalDo(Condition conditional)
        {
            // else has no predicate
            if (conditional.Predicate == null)
            {
                Exec(conditional.Body);
                return;
            }
            MemorySpaces.CreateScope();
            var success = Convert.ToBoolean(Exec(conditional.Predicate));
            if (success)
            {
                Exec(conditional.Body);
                MemorySpaces.PopScope();
            }
            else
            {
                MemorySpaces.PopScope();
                Exec(conditional.Alternate);
            }
        }



        private dynamic Exec(Ast ast)
        {
            try
            {
                if (ast == null)
                {
                    return null;
                }

                if (ast.ConvertedExpression != null)
                {
                    return Exec(ast.ConvertedExpression);
                }

                switch (ast.AstType)
                {
                    case AstTypes.ScopeDeclr:
                        ScopeDelcaration(ast as ScopeDeclr);
                        break;
                    case AstTypes.VarDeclr:
                        VariableDeclaration(ast as VarDeclrAst);
                        break;

                    case AstTypes.Expression:
                        var ret = Expression(ast as Expr);
                        if (ret != null)
                        {
                            return ret;
                        }
                        break;
                    case AstTypes.Print:
                        Print(ast as PrintAst);
                        break;
                    case AstTypes.Create:
                        Create(ast as CreateAst);
                        break;
                    case AstTypes.Delete:
                        Delete(ast as DeleteAst);
                        break;
                    case AstTypes.Copy:
                        Copy(ast as Copy);
                        break;
                    case AstTypes.Move:
                        Move(ast as Move);
                        break;
                    case AstTypes.FindMasks:
                        FindMasks(ast as FindMasks);
                        break;
                    case AstTypes.FindSameFiles:
                        FindSameFiles(ast as FindSameFiles);
                        break;
                    case AstTypes.Backup:
                        Backup(ast as Backup);
                        break;
                    case AstTypes.FunctionInvoke:
                        return InvokeFunction(ast as FuncInvoke);

                    case AstTypes.Conditional:
                        ConditionalDo(ast as Condition);
                        break;
                    case AstTypes.MethodDeclr:
                        var methodDeclr = ast as MethodDeclr;
                        return new MethodSymbol(methodDeclr.Token.TokenVal, ScopeUtil.CreateSymbolType(methodDeclr.ReturnAst), methodDeclr);

                    case AstTypes.While:
                        WhileDo(ast as LoopWhile);
                        break;
                    case AstTypes.Return:
                        ReturnDo(ast as ReturnAst);
                        break;
                    case AstTypes.For:
                        ForDo(ast as LoopFor);
                        break;

                }
            }
            catch (ReturnException ex)
            {
                // let the return value bubble up through all the exectuions until we
                // get to the source syntax tree that invoked it. at that point safely return the value
                if (ast.AstType == AstTypes.FunctionInvoke)
                {
                    return ex.Value;
                }

                throw;
            }

            return null;
        }




        private void ReturnDo(ReturnAst returnAst)
        {
            if (returnAst.ReturnExpression != null)
            {
                var value = Exec(returnAst.ReturnExpression);
                throw new ReturnException(value);
            }
            throw new ReturnException(null);
        }
        private object InvokeFunction(FuncInvoke funcInvoke)
        {
            var method = Resolve(funcInvoke);

            if (method != null)
            {
                var invoker = method as MethodSymbol;

                if (invoker == null)
                {
                    invoker = MemorySpaces.Current.Get(method.Name) as MethodSymbol;
                }

                // args should always be resolved from the current calling space
                // so make sure the invoking function knows which space it comes from
                if (funcInvoke.CallingMemory == null)
                {
                    funcInvoke.CallingMemory = MemorySpaces.Current;
                }

                if (funcInvoke.CallingMemory != null && !CollectionUtil.IsNullOrEmpty(funcInvoke.Arguments))
                {
                    funcInvoke.Arguments.ForEach(arg => arg.CallingMemory = funcInvoke.CallingMemory);
                }

                var oldMemory = MemorySpaces.Current;

                try
                {
                    // if we're a lambda and we have some sort of closure
                    // set our working space to be that. 
                    if (invoker.Environment != null)
                    {
                        MemorySpaces.Current = invoker.Environment;
                    }

                    var value = InvokeMethodSymbol(invoker, funcInvoke.Arguments);

                    return value;
                }
                finally
                {
                    MemorySpaces.Current = oldMemory;
                }
            }


            throw new UndefinedElementException("Undefined method");
        }






        private dynamic GetValue(dynamic value)
        {
            return value is ValueMemory ? (value as ValueMemory).Value : value;
        }

        private object InvokeMethodSymbol(MethodSymbol method, List<Ast> args)
        {
            // create a new memory scope. this is where args will get defined
            // we wont overwrite any memory values since they will all be local here
            MemorySpaces.CreateScope();

            var count = 0;

            if (method.MethodDeclr.Arguments.Count != args.Count)
            {
                throw new InvalidSyntax(String.Format("Wrong number of args passed to method {0}. Got {1}, expected {2}",
                    method.MethodDeclr.MethodName.Token.TokenVal,
                    args.Count, method.MethodDeclr.Arguments.Count));
            }

            foreach (VarDeclrAst expectedArgument in method.MethodDeclr.Arguments)
            {
                var currentArgument = args[count];

                var oldmemory = MemorySpaces.Current;

                // if the argument is coming from somewhere else, make sure to be able to 
                // load the argument from its preferred space. 
                if (currentArgument.CallingMemory != null)
                {
                    MemorySpaces.Current = currentArgument.CallingMemory;
                }

                var value = GetValue(Exec(currentArgument));

                // since we were just loading values from the argument space
                // switch back to the current space so we can assign the argument value
                // into our local working memory
                MemorySpaces.Current = oldmemory;

                if (expectedArgument.VariableValue == null)
                {
                    MemorySpaces.Current.Define(expectedArgument.Token.TokenVal, value);
                }
                else
                {
                    MemorySpaces.Current.Assign(expectedArgument.Token.TokenVal, value);
                }

                // if the passed in argument is a word and not a literal (like string or bool) then 
                // pull its value from memory so we can match type to the target type 
                var resolvedSymbol = (currentArgument.Token.TokenType == TokenType.Word ?
                                                MemorySpaces.Current.Get(currentArgument.Token.TokenVal)
                                            : args[count]) as Symbol;

                var resolvedType = resolvedSymbol != null ? resolvedSymbol.Type : currentArgument.AstSymbolType;

                if (currentArgument is MethodDeclr)
                {
                    resolvedType = new BuiltInType(ExpressionTypes.Method);
                }

                if (!TokenUtil.EqualOrPromotable(expectedArgument.AstSymbolType, resolvedType))
                {
                    throw new InvalidSyntax(String.Format("Cannot pass argument {0} of type {1} to function {2} as argument {3} of type {4}",
                        currentArgument.Token.TokenVal,
                        currentArgument.AstSymbolType.TypeName,
                        method.MethodDeclr.MethodName.Token.TokenVal,
                        expectedArgument.VariableName.Token.TokenVal,
                        expectedArgument.AstSymbolType.TypeName));
                }

                count++;
            }

            var val = Exec(method.MethodDeclr.Body);

            MemorySpaces.PopScope();

            return val;
        }

        private void VariableDeclaration(VarDeclrAst varDeclrAst)
        {
            if (varDeclrAst.VariableValue == null)
            {
                //var symbol = varDeclrAst.CurrentScope.Resolve(varDeclrAst.VariableName.Token.TokenVal);

                //var space = MemorySpaces.Current;

                //space.Define(symbol.Name, TokenType.Nil);

                return;
            }

            var variableValue = varDeclrAst.VariableValue.ConvertedExpression ?? varDeclrAst.VariableValue;

            // if the rhs of a variable is not a method, then execute it, 
            if (variableValue.AstType != AstTypes.MethodDeclr)
            {
                var value = GetValue(Exec(variableValue));

                if (value != null)
                {
                    var symbol = varDeclrAst.CurrentScope.Resolve(varDeclrAst.VariableName.Token.TokenVal);

                    var space = MemorySpaces.Current;

                    if (variableValue.IsLink)
                    {
                        space.Link(symbol.Name, variableValue.Token.TokenVal);
                    }
                    else
                    {
                        space.Define(symbol.Name, value);
                    }
                }
            }
            else
            {
                var symbol = varDeclrAst.CurrentScope.Resolve(varDeclrAst.VariableName.Token.TokenVal);

                var resolvedMethod = varDeclrAst.CurrentScope.Resolve(variableValue.Token.TokenVal) as MethodSymbol;

                // make sure to create a NEW method symbol. this way each time we declare this item
                // it will create a local copy and get its own memory space for closures.  
                // if we shared the same method symbol then all instances of the same declaration would share the memory space, 
                // which may not be what we want given class instances having their own spaces

                var localMethodCopy = new MethodSymbol(resolvedMethod.Name, resolvedMethod.Type,
                                                       resolvedMethod.MethodDeclr);

                var space = MemorySpaces.Current;

                //if (variableValue is LambdaDeclr)
                //{
                //    localMethodCopy.Environment = space;
                //}

                space.Define(symbol.Name, localMethodCopy);
            }
        }

        private void Print(PrintAst ast)
        {


            var expression = GetValue(Exec(ast.Expression));

            MyF.print.WriteLog(expression.ToString());

            

        }
        private void Create(CreateAst ast)
        {
            var expression = GetValue(Exec(ast.Expression));
            FileInfo fInfo = new FileInfo(expression);
            if (fInfo == null)
            {
                MyF.print.WriteLog("\nFile is not created.");
                return;
            }
            if (fInfo.Exists)
            {
                MyF.print.WriteLog("\nFile exist.");
                return;
            }

            File.Create(expression);
            MyF.print.WriteLog("\nFile " + expression + " is created.");
        }
        private void Move(Move ast)
        {
            string path = GetValue(Exec(ast.Arguments.ElementAt(0)));
            string newPath = GetValue(Exec(ast.Arguments.ElementAt(1)));

            FileInfo fileInf = new FileInfo(path);
            if (fileInf.Exists)
            {
                fileInf.MoveTo(newPath);

            }
            MyF.print.WriteLog("\nFile is moved to " + newPath + " .");

        }
        private void Copy(Copy ast)
        {


            string path = GetValue(Exec(ast.Arguments.ElementAt(0)));
            string newPath = GetValue(Exec(ast.Arguments.ElementAt(1)));


            FileInfo fileInf = new FileInfo(path);
            if (fileInf.Exists)
            {
                fileInf.CopyTo(newPath, true);

            }
            MyF.print.WriteLog("\nFile is copied to " + newPath + " .");
        }

        private void FindMasks(FindMasks ast)
        {


            string path = GetValue(Exec(ast.Arguments.ElementAt(0)));
            string mask = GetValue(Exec(ast.Arguments.ElementAt(1)));
            //получть список каталогов на букву D
            //string[] dirs = Directory.GetDirectories(path, mask);
            string[] dirs = Directory.GetFiles(path, mask, SearchOption.AllDirectories);
            if (dirs == null) { MyF.print.WriteLog("\nDo not find"); }
            foreach (string d in dirs)
            {

                MyF.print.WriteLog(d);
                MyF.print.WriteLog("\n");
            }
        }

        public class FileDetails
        {
            public string FileName { get; set; }
            public string FileHash { get; set; }
        }

        private void FindSameFiles(FindSameFiles ast)
        {
            string path = GetValue(Exec(ast.Arguments.ElementAt(0)));
            
            //Get all files from given directory
            var fileLists = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
            int totalFiles = fileLists.Length;

            List<FileDetails> finalDetails = new List<FileDetails>();
            List<string> ToDelete = new List<string>();
            finalDetails.Clear();
            //loop through all the files by file hash code
            foreach (var item in fileLists)
            {
                using (var fs = new FileStream(item, FileMode.Open, FileAccess.Read))
                {
                    finalDetails.Add(new FileDetails()
                    {
                        FileName = item,
                        FileHash = BitConverter.ToString(SHA1.Create().ComputeHash(fs)),
                    });
                }
            }
            //group by file hash code
            var similarList = finalDetails.GroupBy(fInfo => fInfo.FileHash)
                .Select(g => new { FileHash = g.Key, Files = g.Select(z => z.FileName).ToList() });


            //keeping first item of each group as is and identify rest as duplicate files to delete
            ToDelete.AddRange(similarList.SelectMany(fInfo => fInfo.Files.Skip(1)).ToList());
            MyF.print.WriteLog("Total duplicate files " +ToDelete.Count);
            foreach (string d in ToDelete)
            {

                MyF.print.WriteLog(d);
                MyF.print.WriteLog("\n");
            }
        }


        private void Backup(Backup ast)
        {


            string openedFilename = GetValue(Exec(ast.Arguments.ElementAt(0)));

            if (File.Exists(openedFilename))
                File.Copy(openedFilename, openedFilename.Replace(".txt", " backup.txt"), true);
            MyF.print.WriteLog("\nBackup file was created.");

        }

    

        private void Delete(DeleteAst ast)
        {
            var expression = GetValue(Exec(ast.Expression));
           // if (expression.Exists)
           // {
                File.Delete(expression);
            //}
            MyF.print.WriteLog("File " + expression + " is deleted.");
        }

        private void Assign(Ast ast, dynamic value, MemorySpace space = null)
        {
            if (value is ValueMemory)
            {
                var tup = value as ValueMemory;

                tup.Memory.Assign(ast.Token.TokenVal, tup.Value);

                return;
            }

            if (space != null)
            {
                space.Assign(ast.Token.TokenVal, value);
                return;
            }

            MemorySpaces.Current.Assign(ast.Token.TokenVal, value);
        }

        private ValueMemory Get(Ast ast)
        {
            object item;

            if (Environment.Count > 0)
            {
                foreach (var env in Environment)
                {
                    item = env.Get(ast.Token.TokenVal, true);
                    if (item != null)
                    {
                        //return item;
                        return new ValueMemory(item, env);
                    }
                }
            }

            item = MemorySpaces.Current.Get(ast.Token.TokenVal);

            if (item != null)
            {
                return new ValueMemory(item, MemorySpaces.Current);
            }

            if (ast.CallingMemory != null)
            {
                return new ValueMemory(ast.CallingMemory.Get(ast.Token.TokenVal), ast.CallingMemory);
            }




            return null;
        }

        private dynamic Expression(Expr ast)
        {
            var lhs = ast.Left;
            var rhs = ast.Right;

            switch (ast.Token.TokenType)
            {
                case TokenType.Equals:
                    if (lhs.AstType == AstTypes.ClassRef)
                    {
                     
                    }

                    else
                    {
                        ValueMemory itemSpace = Get(lhs);

                        Assign(lhs, Exec(rhs), itemSpace != null ? itemSpace.Memory : null);
                    }
                    return null;

                case TokenType.Word:
                    return Get(ast);

                case TokenType.Int:
                    return Convert.ToInt32(ast.Token.TokenVal);

                case TokenType.Float:
                    return Convert.ToDouble(ast.Token.TokenVal);

                case TokenType.QuotedString:
                    return ast.Token.TokenVal;

                case TokenType.Nil:
                    return TokenType.Nil;

                case TokenType.True:
                    return true;

                case TokenType.False:
                    return false;
            }

            if (TokenUtil.IsOperator(ast.Token))
            {
                return ApplyOperation(ast);
            }

            return null;
        }


        private object ApplyOperation(Expr ast)
        {
            dynamic leftExec = Exec(ast.Left);

            var left = leftExec is ValueMemory ? (leftExec as ValueMemory).Value : leftExec;
            // short circuit
            if (ast.Token.TokenType == TokenType.Or)
            {
                if (left is bool && left == true)
                {
                    return true;
                }
            }

            if (ast.Token.TokenType == TokenType.Compare)
            {
                if (left is bool && left == false)
                {
                    return false;
                }
            }

            dynamic rightExec = Exec(ast.Right);


            var right = rightExec is ValueMemory ? (rightExec as ValueMemory).Value : rightExec;

            switch (ast.Token.TokenType)
            {
                case TokenType.Compare:
                    if (left is TokenType || right is TokenType)
                    {
                        return NullTester.NullEqual(left, right);
                    }

                    return left == right;
                case TokenType.NotCompare:
                    if (left is TokenType || right is TokenType)
                    {
                        return !NullTester.NullEqual(left, right);
                    }
                    return left != right;

                case TokenType.GreaterThan:
                    return left > right;
                case TokenType.LessThan:
                    return left < right;
                case TokenType.Plus:
                    return left + right;
                case TokenType.Minus:
                    return left - right;
                case TokenType.Slash:
                    return left / right;
                case TokenType.Carat:
                    return left ^ right;
                case TokenType.Ampersand:
                    return left && right;
                case TokenType.Or:
                    return left || right;
            }

            return null;
        }


        private void ScopeDelcaration(ScopeDeclr ast)
        {
            MemorySpaces.CreateScope();

            ast.ScopedStatements.ForEach(statement => statement.Execute(this));

            MemorySpaces.PopScope();
        }

        private Symbol Resolve(Ast ast)
        {
            if (ast == null)
            {
                return null;
            }

            var resolved = ast.CurrentScope.Resolve(ast.Token.TokenVal);

            if (resolved == null)
            {
                resolved = ast.Global.Resolve(ast.Token.TokenVal);

                if (resolved == null)
                {
                    var msg = String.Format("Trying to access undefined function {0}", ast.Token.TokenVal);

                    MyF.print.WriteLog(msg);

                    throw new UndefinedElementException(msg);
                }
            }

            return resolved;
        }
    }

    public class ScopeBuilderExecuter : IAstExecuter
    {
        private void SetScopeType(ScopeType scopeType)
        {
            ScopeContainer.CurrentScopeType = scopeType;
        }

        private Scope Global { get; set; }

        private Scope Current
        {
            get { return ScopeTree.Current; }
            set { ScopeContainer.CurrentScopeStack.Current = value; }
        }

        private ScopeStack<Scope> ScopeTree { get { return ScopeContainer.CurrentScopeStack; } }

        private MethodDeclr CurrentMethod { get; set; }

        private Boolean ResolvingTypes { get; set; }

        private ScopeContainer ScopeContainer { get; set; }

        public ScopeBuilderExecuter(bool resolvingTypes = false)
        {
            ResolvingTypes = resolvingTypes;
            ScopeContainer = new ScopeContainer();
        }

        public void Execute(Condition ast)
        {
            if (ast.Predicate != null)
            {
                ast.Predicate.Execute(this);
            }

            ast.Body.Execute(this);

            if (ast.Alternate != null)
            {
                ast.Alternate.Execute(this);
            }

            SetScope(ast);
        }

        public void Execute(Expr ast)
        {
            if (ast.Left != null)
            {
                ast.Left.Execute(this);
            }

            if (ast.Right != null)
            {
                ast.Right.Execute(this);
            }

            SetScope(ast);

            if (ast.Left == null && ast.Right == null)
            {
                ast.AstSymbolType = ResolveOrDefine(ast);
            }
            else
            {
                if (ResolvingTypes)
                {
                    ast.AstSymbolType = GetExpressionType(ast.Left, ast.Right, ast.Token);
                }
            }
        }


        private IType ResolveOrDefine(Expr ast)
        {
            if (ast == null)
            {
                return null;
            }

            switch (ast.Token.TokenType)
            {
                case TokenType.Word: return ResolveType(ast);
            }

            return ScopeUtil.CreateSymbolType(ast);
        }

    
        private IType GetExpressionType(Ast left, Ast right, Token token)
        {
            switch (token.TokenType)
            {
                case TokenType.Ampersand:
                case TokenType.Or:
                case TokenType.GreaterThan:
                case TokenType.Compare:
                case TokenType.LessThan:
                case TokenType.NotCompare:
                    return new BuiltInType(ExpressionTypes.Boolean);

                case TokenType.Method:
                case TokenType.Infer:
                    if (right is MethodDeclr)
                    {
                        return new BuiltInType(ExpressionTypes.Method, right);
                    }

                    return right.AstSymbolType;
            }

            if (!ResolvingTypes && (left.AstSymbolType == null || right.AstSymbolType == null))
            {
                return null;
            }

        
            return left.AstSymbolType;
        }

        public void Execute(Move ast)
        {

        }

        public void Execute(Copy ast)
        {

        }
        public void Execute(FindMasks ast)
        {

        }
        public void Execute(FindSameFiles ast)
        {

        }
        public void Execute(Backup ast)
        {

        }
        public void Execute(FuncInvoke ast)
        {
            if (ast.CallingScope != null)
            {
                ast.Arguments.ForEach(arg => arg.CallingScope = ast.CallingScope);
            }

            ast.Arguments.ForEach(arg => arg.Execute(this));

            SetScope(ast);

            var functionType = Resolve(ast.FunctionName) as MethodSymbol;

            if (functionType != null && ast.Arguments.Count < functionType.MethodDeclr.Arguments.Count)
            {

            }
            else if (ResolvingTypes)
            {
                ast.AstSymbolType = ResolveType(ast.FunctionName, ast.CurrentScope);
            }
        }





 
        private IType ResolveType(Ast ast, Scope currentScope = null)
        {
            var scopeTrys = new List<Scope> { currentScope, ast.CurrentScope };

            try
            {
                return Current.Resolve(ast).Type;
            }
            catch (Exception ex)
            {
                try
                {
                    return ast.CallingScope.Resolve(ast).Type;
                }
                catch
                {
                    foreach (var scopeTry in scopeTrys)
                    {
                        try
                        {
                            if (scopeTry == null)
                            {
                                continue;
                            }

                            var resolvedType = scopeTry.Resolve(ast);

                            var allowedFwdReferences = scopeTry.AllowedForwardReferences(ast);

                            if (allowedFwdReferences ||
                                scopeTry.AllowAllForwardReferences ||
                                resolvedType is ClassSymbol ||
                                resolvedType is MethodSymbol)
                            {
                                return resolvedType.Type;
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }

            if (ResolvingTypes)
            {
                if (ast.IsPureDynamic)
                {
                    return new BuiltInType(ExpressionTypes.Inferred);
                }

                throw new UndefinedElementException(String.Format("Undefined element {0}",
                                                                          ast.Token.TokenVal));
            }

            return null;
        }


        private Symbol Resolve(String name)
        {
            try
            {
                return Current.Resolve(name);
            }
            catch (Exception ex)
            {
                try
                {
                    return Global.Resolve(name);
                }
                catch
                {
                    if (ResolvingTypes)
                    {
                        return null;
                    }
                }

                throw;
            }
        }

        private Symbol Resolve(Ast ast)
        {
            return Resolve(ast.Token.TokenVal);
        }

        public void Execute(VarDeclrAst ast)
        {
            var isVar = ast.DeclarationType.Token.TokenType == TokenType.Infer;

            if (ast.DeclarationType != null && !isVar)
            {
                var symbol = ScopeUtil.DefineUserSymbol(ast.DeclarationType, ast.VariableName);

                //symbol.IsArray = ast is ArrayDeclrAst;

                DefineToScope(ast, symbol);

                ast.AstSymbolType = symbol.Type;
            }

            if (ast.VariableValue == null && isVar)
            {
                var symbol = ScopeUtil.DefineUserSymbol(ast.DeclarationType, ast.VariableName);

                DefineToScope(ast, symbol);

                ast.AstSymbolType = symbol.Type;
            }

            else if (ast.VariableValue != null)
            {
                ast.VariableValue.Execute(this);

                // if its type inferred, determine the declaration by the value's type
                if (isVar)
                {
                    // if the right hand side is a method declaration, make sure to track the source value
                    // this way we can reference it later to determine not only that this is a method type, but what
                    // is the expected return value for static type checking later

                    var val = ast.VariableValue.ConvertedExpression ?? ast.VariableValue;

                    ast.AstSymbolType = val is MethodDeclr
                                            ? new BuiltInType(ExpressionTypes.Method, val)
                                            : val.AstSymbolType;

                    var symbol = ScopeUtil.DefineUserSymbol(ast.AstSymbolType, ast.VariableName);

                    //symbol.IsArray = ast is ArrayDeclrAst;

                    DefineToScope(ast, symbol);
                }
                else if (ResolvingTypes)
                {
                    var declaredType = ScopeUtil.CreateSymbolType(ast.DeclarationType);

                    var value = ast.VariableValue.ConvertedExpression ?? ast.VariableValue;

                    ReturnAst returnType = null;

                    // when we're resolving types check if the rhs is a function invoke. if it is, see 
                    // what the return value of the src expression is so we can make sure that the 
                    // lhs and the rhs match.
                    try
                    {
                        returnType =
                            value is FuncInvoke
                                ? ((value as FuncInvoke).AstSymbolType) != null
                                      ? ((value as FuncInvoke).AstSymbolType.Src as MethodDeclr).ReturnAst
                                      : null
                                : null;

                    }
                    catch
                    {
                    }

                    value = returnType != null ? returnType.ReturnExpression : value;

                    if (!TokenUtil.EqualOrPromotable(value.AstSymbolType.ExpressionType, declaredType.ExpressionType))
                    {
                        throw new InvalidSyntax(String.Format("Cannot assign {0} of type {1} to {2}", ast.VariableValue,
                            value.AstSymbolType.ExpressionType,
                            declaredType.ExpressionType));
                    }

                }
            }

            SetScope(ast);
        }

        private void DefineToScope(Ast ast, Symbol symbol)
        {
            if (ast.CurrentScope != null && ast.CurrentScope.Symbols.ContainsKey(symbol.Name))
            {
                Symbol old = ast.CurrentScope.Resolve(symbol.Name);
                if (old.Type == null)
                {
                    ast.CurrentScope.Define(symbol);
                }
            }

            Current.Define(symbol);
        }

        public void Execute(MethodDeclr ast)
        {
            var previousMethod = CurrentMethod;

            CurrentMethod = ast;

            var symbol = ScopeUtil.DefineMethod(ast);

            Current.Define(symbol);

            ScopeTree.CreateScope();

            ast.Arguments.ForEach(arg => arg.Execute(this));

            ast.Body.Execute(this);

            SetScope(ast);

            if (symbol.Type.ExpressionType == ExpressionTypes.Inferred)
            {
                if (ast.ReturnAst == null)
                {
                    ast.AstSymbolType = new BuiltInType(ExpressionTypes.Void);
                }
                else
                {
                    ast.AstSymbolType = ast.ReturnAst.AstSymbolType;
                }
            }
            else
            {
                ast.AstSymbolType = symbol.Type;
            }

            ValidateReturnStatementType(ast, symbol);

            ScopeTree.PopScope();

            CurrentMethod = previousMethod;
        }

        private void ValidateReturnStatementType(MethodDeclr ast, Symbol symbol)
        {
            if (!ResolvingTypes)
            {
                return;
            }

            IType returnStatementType;

            // no return found
            if (ast.ReturnAst == null)
            {
                returnStatementType = new BuiltInType(ExpressionTypes.Void);
            }
            else
            {
                returnStatementType = ast.ReturnAst.AstSymbolType;
            }

            var delcaredSymbol = ScopeUtil.CreateSymbolType(ast.MethodReturnType);

            // if its inferred, just use whatever the return statement i
            if (delcaredSymbol.ExpressionType == ExpressionTypes.Inferred)
            {
                return;
            }

            if (!TokenUtil.EqualOrPromotable(returnStatementType.ExpressionType, delcaredSymbol.ExpressionType))
            {
                throw new InvalidSyntax(String.Format("Return type {0} for function {1} is not of the same type of declared method (type {2})",
                    returnStatementType.ExpressionType, symbol.Name, delcaredSymbol.ExpressionType));
            }
        }

        public void Execute(LoopWhile ast)
        {
            ast.Predicate.Execute(this);

            ast.Body.Execute(this);

            SetScope(ast);
        }

        public void Execute(ScopeDeclr ast)
        {
            ScopeTree.CreateScope();

            ast.ScopedStatements.ForEach(statement => statement.Execute(this));

            SetScope(ast);

            ScopeTree.PopScope();
        }

        private void SetScope(Ast ast)
        {
            if (ast.CurrentScope == null)
            {
                ast.CurrentScope = Current;

                ast.Global = Global;
            }

            if (ast.CurrentScope != null && ast.CurrentScope.Symbols.Count < Current.Symbols.Count)
            {
                ast.CurrentScope = Current;
            }

            if (ast.Global != null && ast.Global.Symbols.Count < Global.Symbols.Count)
            {
                ast.Global = Global;
            }
        }

        public void Execute(LoopFor ast)
        {
            ast.Setup.Execute(this);

            ast.Predicate.Execute(this);

            if (ResolvingTypes && ast.Predicate.AstSymbolType.ExpressionType != ExpressionTypes.Boolean)
            {
                throw new InvalidSyntax("For loop predicate has to evaluate to a boolean");
            }

            ast.Update.Execute(this);

            ast.Body.Execute(this);

            SetScope(ast);
        }

        public void Execute(ReturnAst ast)
        {
            if (ast.ReturnExpression != null)
            {
                ast.ReturnExpression.Execute(this);

                ast.AstSymbolType = ast.ReturnExpression.AstSymbolType;

                CurrentMethod.ReturnAst = ast;
            }
        }

        public void Execute(CreateAst ast)
        {
            ast.Expression.Execute(this);

        }
        public void Execute(DeleteAst ast)
        {
            ast.Expression.Execute(this);

        }

        public void Execute(PrintAst ast)
        {
            ast.Expression.Execute(this);

            if (ResolvingTypes)
            {
                if (ast.Expression.AstSymbolType == null)
                {
                    throw new InvalidSyntax("Undefined expression in print statement");
                }

                if (ast.Expression.AstSymbolType.ExpressionType == ExpressionTypes.Void)
                {
                    throw new InvalidSyntax("Cannot print a void expression");
                }

                if (ast.Expression.AstSymbolType.ExpressionType == ExpressionTypes.Method)
                {
                    var returnAst = (ast.Expression.AstSymbolType.Src as MethodDeclr);

                    if (returnAst != null)
                    {
                        var retStatement = returnAst.ReturnAst;

                        if (retStatement == null)
                        {
                            throw new InvalidSyntax("Cannot print a void expression");
                        }
                    }

                }
            }
        }

        public void Start(Ast ast)
        {


            if (ast.Global != null)
            {
                Global = ast.Global;
            }

            ast.Execute(this);
        }



        private void SetScopeSource(Symbol classSymbol)
        {
            Current = classSymbol;
        }


    }


    public class PrintAstExecuter : IAstExecuter
    {
        public void Execute(Condition ast)
        {
            Console.Write(ast.Token);

            if (ast.Predicate != null)
            {
                PrintWrap("Predicate", () => ast.Predicate.Execute(this));
            }

            ast.Body.Execute(this);

            if (ast.Alternate != null)
            {
                if (ast.Alternate.Token.TokenType == TokenType.If)
                {
                    Console.Write("Else");
                }

                ast.Alternate.Execute(this);
            }
        }

        public void Execute(Expr ast)
        {
            if (ast.Left != null)
            {
                ast.Left.Execute(this);
            }

            Console.Write(" " + ast.Token);

            if (ast.Right != null)
            {
                Console.Write(" ");

                ast.Right.Execute(this);

                Console.WriteLine();
            }
        }

        public void Execute(FuncInvoke ast)
        {
            ast.FunctionName.Execute(this);

            ast.Arguments.ForEach(arg => arg.Execute(this));
        }
        public void Execute(Move ast)
        {
        }
        public void Execute(Copy ast)
        {
        }
        public void Execute(FindMasks ast)
        {
        }
        public void Execute(FindSameFiles ast)
        {
        }
        public void Execute(Backup ast)
        {
        }
        public void Execute(VarDeclrAst ast)
        {
            if (ast.DeclarationType != null)
            {
                ast.DeclarationType.Execute(this);
            }

            ast.VariableName.Execute(this);

            if (ast.VariableValue != null)
            {
                MyF.print.WriteLog("Equals");

                ast.VariableValue.Execute(this);
            }
        }
        public void Execute(MethodDeclr ast)
        {
            PrintWrap("MethodDeclaration", () =>
            {
                PrintWrap("Return type", () => ast.MethodReturnType.Execute(this));

                PrintWrap("FunctionName", () => ast.MethodName.Execute(this));

                PrintWrap("Arguments", () => ast.Arguments.ForEach(arg => arg.Execute(this)));

                PrintWrap("Body", () => ast.Body.Execute(this));
            });
        }

        public void Execute(LoopWhile ast)
        {
            Console.Write(ast.Token);

            PrintWrap("Predicate", () => ast.Predicate.Execute(this));

            ast.Body.Execute(this);
        }

        public void Execute(ScopeDeclr ast)
        {
            PrintWrap("Scope", () => ast.ScopedStatements.ForEach(statement => statement.Execute(this)), true);
        }

        public void Execute(LoopFor ast)
        {
            PrintWrap("LoopFor", () =>
            {
                PrintWrap("For", () => ast.Setup.Execute(this));
                PrintWrap("Until", () => ast.Predicate.Execute(this));
                PrintWrap("Modify", () => ast.Update.Execute(this));

                ast.Body.Execute(this);
            });
        }

        public void Execute(ReturnAst ast)
        {
            PrintWrap("Return", () =>
            {
                if (ast.ReturnExpression != null)
                {
                    ast.ReturnExpression.Execute(this);
                }
            });
        }

        public void Execute(PrintAst ast)
        {
            PrintWrap("Print", () => ast.Expression.Execute(this));
        }

        public void Execute(CreateAst ast)
        {
            PrintWrap("Create", () => ast.Expression.Execute(this));
        }
        public void Execute(DeleteAst ast)
        {
            PrintWrap("Delete", () => ast.Expression.Execute(this));
        }

        public void Start(Ast ast)
        {
            ast.Execute(this);
        }

        private void PrintWrap(string name, Action action, bool newLine = false)
        {
            if (newLine)
            {
                MyF.print.WriteLog(name + " (");
            }
            else
            {
                MyF.print.WriteLog(name + " (");
            }


            action();

            MyF.print.WriteLog(" )");
        }
    }
}
