﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Language.Lexers;
using Language.Parser;
using Language.Executer;
using System.IO;
using System.Timers;

namespace Interpreter
{
    public partial class MyF : System.Windows.Forms.Form
    {
        public static MyF print;
        public static MyF cl;
        public MyF()
        {
            InitializeComponent();
            print = this;
            cl = this;
        }
        public void WriteLog(string log)
        {
            this.Invoke(new Action(() => { textBox2.Text += log+Environment.NewLine;  }));

        }
        public void Clearing()
        {
            this.Invoke(new Action(() => { textBox2.Text = ""; textBox2.Clear(); }));

        }

        public class EmpryLine
        {
            public void Msg() {  MyF.print.WriteLog("The line is empty. Try to do it again") ; }
        }

        public class ClearLine
        {
            public void ClearLines() { MyF.cl.Clearing(); }
        }


        public delegate void Empty();
        public event Empty emptyLine;

        public delegate void Clear();
        public event Clear clear;

        private void button1_Click(object sender, EventArgs e)
        {
            EmpryLine ev = new EmpryLine();
            emptyLine += ev.Msg;
            ClearLine clea = new ClearLine();
            clear += clea.ClearLines;
            textBox2.Text = "";


            var str = textBox1.Text;

            
            if (str == null || str == "")
            {
                emptyLine();
            }

            

            var ast = new LanguageParser(new Lexer(str)).Parse();
            new InterpretorExecuter().Start(ast);

        }

        private void Form_Load(object sender, EventArgs e)
        {

        }

        private void MyF_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        private void MyF_Activated(object sender, EventArgs e)
        {
 
        }


        private void MyF_Click(object sender, EventArgs e)
        {
            

        }

        private void MyF_Load(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
