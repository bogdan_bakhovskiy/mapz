﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Language.Other;

namespace Language.Data
{
    public class Token
    {
        public TokenType TokenType { get; private set; }

        public String TokenVal { get; private set; }

        public Token(TokenType tokenType, String token)
        {
            TokenType = tokenType;
            TokenVal = token;
        }

        public Token(TokenType tokenType)
        {
            TokenVal = null;
            TokenType = tokenType;
        }

        public override string ToString()
        {
            return TokenType + ": " + TokenVal;
        }
    }

    public enum TokenType
    {
        DeRef,
        Ampersand,
        Fun,
        GreaterThan,
        LessThan,
        SemiColon,
        If,
        Return,
        While,
        Else,
        ScopeStart,
        EOF,
        For,
        Infer,
        Void,
        WhiteSpace,
        LBracket,
        RBracket,
        Plus,
        Minus,
        Equals,
        HashTag,
        QuotedString,
        Word,
        Comma,
        OpenParenth,
        CloseParenth,
        Asterix,
        Slash,
        Carat,
        Float,
        Print,
        Dot,
        True,
        False,
        Boolean,
        Or,
        Int,
        Double,
        String,
        Method,

        Compare,
        Nil,
        NotCompare,

       
        Copy,
        FindMasks,
        FindSameFiles,
        LSquareBracket,
        RSquareBracket,
        Create,
        Delete,
        Move,
        Backup

    }

    class ValueMemory
    {
        public dynamic Value { get; set; }
        public MemorySpace Memory { get; set; }

        public ValueMemory(dynamic val, MemorySpace mem)
        {
            Value = val;
            Memory = mem;
        }
    }
}
