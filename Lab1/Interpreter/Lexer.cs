﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

using Language.Other;
using Language.Parser;
using Language.Data;
using Language.Matches;
using Language.AST;
using Language.Exceptions;
using Language.Executer;
using System.Windows.Forms;
using Interpreter;




namespace Language.Lexers
{
    public class Tokenizer : TokenizableStreamBase<String>
    {
        public Tokenizer(String source)
            : base(() => source.ToCharArray().Select(i => i.ToString(CultureInfo.InvariantCulture)).ToList())
        {

        }
    }

    public class TokenizableStreamBase<T> where T : class
    {
        public TokenizableStreamBase(Func<List<T>> extractor)
        {
            Index = 0;

            Items = extractor();

            SnapshotIndexes = new Stack<int>();
        }

        private List<T> Items { get; set; }

        protected int Index { get; set; }

        private Stack<int> SnapshotIndexes { get; set; }

        public virtual T Current
        {
            get
            {
                if (EOF(0))
                {
                    return null;
                }

                return Items[Index];
            }
        }

        public void Consume()
        {
            Index++;
        }

        private Boolean EOF(int lookahead)
        {
            if (Index + lookahead >= Items.Count)
            {
                return true;
            }

            return false;
        }

        public Boolean End()
        {
            return EOF(0);
        }

        public virtual T Peek(int lookahead)
        {
            if (EOF(lookahead))
            {
                return null;
            }

            return Items[Index + lookahead];
        }

        public void TakeSnapshot()
        {
            SnapshotIndexes.Push(Index);
        }

        public void RollbackSnapshot()
        {
            Index = SnapshotIndexes.Pop();
        }

        public void CommitSnapshot()
        {
            SnapshotIndexes.Pop();
        }
    }
    public class Lexer
    {
        private Tokenizer Tokenizer { get; set; }

        private List<IMatcher> Matchers { get; set; }

        public Lexer(String source)
        {
            Tokenizer = new Tokenizer(source);
        }
       
        public IEnumerable<Token> Lex()
        {
           
            Matchers = InitializeMatchList();

            var current = Next();


            while (current != null && current.TokenType != TokenType.EOF)
            {
                
                if (current.TokenType != TokenType.WhiteSpace)
                {
                    yield return current;
                }

                current = Next();
                string k = current.ToString();
             
             



            }

        }
       

        private List<IMatcher> InitializeMatchList()
        {
            // the order here matters because it defines token precedence

            var matchers = new List<IMatcher>(64);

            var keywordmatchers = new List<IMatcher>
                                  {
                                      new MatchKeyword(TokenType.Void, "void"),
                                      new MatchKeyword(TokenType.Int, "int"),
                                      new MatchKeyword(TokenType.If, "if"),
                                      new MatchKeyword(TokenType.Infer, "var"),
                                      new MatchKeyword(TokenType.Else, "else"),
                                      new MatchKeyword(TokenType.While, "while"),
                                      new MatchKeyword(TokenType.For, "for"),
                                      new MatchKeyword(TokenType.Return, "return"),
                                      new MatchKeyword(TokenType.Print, "print"),
                                      new MatchKeyword(TokenType.True, "true"),
                                      new MatchKeyword(TokenType.False, "false"),
                                      new MatchKeyword(TokenType.Boolean, "bool"),
                                      new MatchKeyword(TokenType.String, "string"),
                                      new MatchKeyword(TokenType.Method, "method"),
                                      new MatchKeyword(TokenType.Create, "Create"),
                                      new MatchKeyword(TokenType.Copy,  "Copy"),
                                      new MatchKeyword(TokenType.Move, "Move"),
                                      new MatchKeyword(TokenType.FindMasks, "FindMasks"),
                                       new MatchKeyword(TokenType.FindSameFiles, "FindSameFiles"),
                                 
                                       new MatchKeyword(TokenType.Delete, "Delete"),
                                       new MatchKeyword(TokenType.Backup, "Backup")
                                  };


            var specialCharacters = new List<IMatcher>
                                    {

                                        new MatchKeyword(TokenType.LBracket, "{"),
                                        new MatchKeyword(TokenType.RBracket, "}"),
                                       
                                   
                                        new MatchKeyword(TokenType.Plus, "+"),
                                        new MatchKeyword(TokenType.Minus, "-"),
                                        new MatchKeyword(TokenType.NotCompare, "!="),
                                        new MatchKeyword(TokenType.Compare, "=="),
                                        new MatchKeyword(TokenType.Equals, "="),

                                        new MatchKeyword(TokenType.Comma, ","),
                                        new MatchKeyword(TokenType.OpenParenth, "("),
                                        new MatchKeyword(TokenType.CloseParenth, ")"),
                                        new MatchKeyword(TokenType.Asterix, "*"),
                                        new MatchKeyword(TokenType.Slash, "/"),
                                       

                                        new MatchKeyword(TokenType.GreaterThan, ">"),
                                        new MatchKeyword(TokenType.LessThan, "<"),
                                        new MatchKeyword(TokenType.Or, "||"),
                                        new MatchKeyword(TokenType.SemiColon, ";"),
                                        new MatchKeyword(TokenType.Dot, "."),
                                    };

            // give each keyword the list of possible delimiters and not allow them to be 
            // substrings of other words, i.e. token fun should not be found in string "function"
            keywordmatchers.ForEach(keyword =>
            {
                var current = (keyword as MatchKeyword);
                current.AllowAsSubString = false;
                current.SpecialCharacters = specialCharacters.Select(i => i as MatchKeyword).ToList();
            });

            matchers.Add(new MatchString(MatchString.QUOTE));
            matchers.Add(new MatchString(MatchString.TIC));
            matchers.AddRange(specialCharacters);
            matchers.AddRange(keywordmatchers);
            matchers.AddRange(new List<IMatcher>
                                                {
                                                    new MatchWhiteSpace(),
                                                    new MatchNumber(),
                                                    new MatchWord(specialCharacters)
                                                });

            return matchers;
        }

        private Token Next()
        {
            if (Tokenizer.End())
            {
                return new Token(TokenType.EOF);
            }

            return
                 (from match in Matchers
                  let token = match.IsMatch(Tokenizer)
                  where token != null
                  select token).FirstOrDefault();
        }
    }
   
}
